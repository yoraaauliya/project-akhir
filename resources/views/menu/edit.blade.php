@extends('layout.master')
@section('judul')
Halaman edit Kategori
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')

    <div class="form-group">
        <label >Nama Menu :</label>
        <input type="text" value="{{$cast->namaMenu}}"class="form-control" name="namaMenu">
      </div>
      @error('namaMenu')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label >Tipe Menu :</label>
        <input type="text" value="{{$cast->tipeMenu}}" class="form-control" name="tipeMenu">
      </div>
      @error('tipeMenu')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
          <label >Bahan Utama :</label>
          <input type="text" value="{{$cast->bahanUtama}}"class="form-control" name="bahanUtama">
      </div>
      @error('bahanUtama')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
          <label >harga Menu :</label>
          <input type="number" value="{{$cast->hargaMenu}}" class="form-control" name="hargaMenu">
      </div>
      @error('hargaMenu')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
          <label >informasi Makanan :</label>
          <input type="text" value="{{$cast->informasiMakanan}}" class="form-control" name="informasiMakanan">
      </div>
      @error('informasiMakanan')
          <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection