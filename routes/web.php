<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Pengguna.login');
})->name('login');



Route::post('/postlogin', 'LoginController@postlogin')->name('postlogin');
Route::get('/logout', 'LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth']],function () {
    Route::get('/cast','MenuControlle@index');
});

Route::get('/cast/create', 'MenuController@create');
Route::post('/cast', 'MenuController@store');


Route::get('/cast', 'MenuController@index');

Route::get('/castdb', 'MenuController@dashboard');


Route::get('/cast/{cast_id}', 'MenuController@show');

Route::get('/cast/{cast_id}/edit', 'MenuController@edit');

Route::put('/cast/{cast_id}', 'MenuController@update');
Route::delete('/cast/{cast_id}', 'MenuController@destroy');
